<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

	

    <title>Hello, world!</title>
  </head>


<?php
// Config
$host = 'localhost';
$port = 27017;
$dbname = 'new_york';

 
// Connect to database
$conn = new MongoDB\Driver\Manager("mongodb://$host:$port");

 
$id = new \MongoDB\BSON\ObjectId("5dc16f25f504a468acb39689");
$filter = ['fields.name' => 'Concerts -> Classique'];
$options = [];

$query = new \MongoDB\Driver\Query([]);
$rows   = $conn->executeQuery('CartePostale.HauteSeine', $query);


$i = 0;
foreach ($rows as $document) {
    echo $document->_id. "\n";


    if(++$i > 20) break;


}

 
?>