<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="../css/style.css">
    <link href="../lightbox2-2.11.1/dist/css/lightbox.css" rel="stylesheet" 
	/>
	<link href="../lightbox2-2.11.1/dist/css/lightbox.min.css" rel="stylesheet" />

	<script src="../lightbox2-2.11.1/dist/js/lightbox.js"></script>
    <script src="../lightbox2-2.11.1/dist/js/lightbox-plus-jquery.min.js"></script>

    <title>Accueil</title>
  </head>

<?php

include 'activite.php';
include 'menu.php';



$activite = new Activite();
$resultatQuery = $activite->getDocumentById($_POST["idDocument"]);

echo('<div class="conteneur-document">');
foreach ($resultatQuery as $document) {
	
	echo('
		
		<div class="document">
			<a id="linkPicture" class="example-image-link" href="'.$document->images.'" data-lightbox="example-1">
				<img src="'.$document->images.'" class="img-document" alt="...">
			</a>
			<div class="document-content">
				<p class="document-titre">'.$document->Titre.'</p>
				<p class="document-commune">'.$document->Commune.'</p>
				<p class="document-date">'.$document->Date.'</p>
			</div>
			<p class="document-theme">Thèmes : '.$document->Thèmes.'</p>

		</div>
		
		');
}

echo('

	<div class="googleMaps">
		<iframe 
		  width="100%" 
		  height="100%" 
		  frameborder="0" 
		  scrolling="no" 
		  marginheight="0" 
		  marginwidth="0" 
		  src="https://maps.google.com/maps?q='.$document->geo_point_2d.'&hl=es&z=14&amp;output=embed">
		 </iframe>
	</div>
	
	');


echo('</div>');

include 'footer.php';

?>