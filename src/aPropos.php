<html>

<html><head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="../css/style.css">

    <title>Accueil</title>
  </head>

<?php

include 'activite.php';
include 'menu.php';

echo('<body>');
echo('
<p class="content-apropos"> Cartes postales d\'époque (1900-1944) consacrées au territoire des Hauts-de-Seine conservées aux Archives départementales.<br/><br/><br/>

Les Archives départementales des Hauts-de-Seine conservent une importante collection de cartes postales constituée essentiellement à partir d’acquisitions.<br/>

Ce fonds de près de 10 500 cartes postales, illustrant les paysages urbains et champêtres des 36 communes des Hauts-de-Seine ainsi que les différents modes de locomotion à travers le temps, se compose de 3 sous-séries :<br/><br/>
<ul>
<li>9 Fi, Cartes postales anciennes (1900-1944),<br/>
<li>10 Fi, Cartes postales postérieures à la Seconde Guerre mondiale (1945 à nos jours)<br/>
<li>12 Fi 2, Collection Lapie (1950-1955).<br/><br/>
Ces cartes postales, classées par communes et par thèmes, évoquent le passé de nos 36 communes : scènes de rues, marchés animés, promenades d’élégantes dans les bois ou les parcs. Le thème des transports y est largement représenté avec le chemin de fer, le tramway, l’automobile ou encore l’aviation.<br/>

Les Archives départementales proposent dans ce jeu de données la sous-série de cartes postales anciennes (9 Fi) consacrée à la période 1900 à 1944, sous-série tombée dans le domaine public. Cette collection mise en ligne est amenée à s’enrichir régulièrement au fur et à mesure de l’avancée des opérations de numérisation de ce fonds iconographique.<br/><br/>

	<B>Information Importante : Les données du site proviennent du site web open data</B> <a href="https://opendata.hauts-de-seine.fr/explore/dataset/cartes-postales/information/?sort=commune">lien</a>.<br/>
	Les données utilisées sont en open source.
	Actuellement il existe un autre site web de bien meilleur qualité que le mien :) pour visualiser les cartes postales des Haute de Seine
	<a href="http://hauts-de-seine.maps.arcgis.com/apps/Shortlist/index.html?appid=8fb11c960527496d9e7ee9abeb9ddc7b"> lien</a>.

</p>
');
echo('</body>');

include 'footer.php';

?>