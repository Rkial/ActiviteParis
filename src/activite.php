<?php

class Activite
{
 

  private $_connexionMongo;
  private $_host = "localhost";
  private $_port = 27017;
  private $_bdd = "CartePostale";
  private $_collection = "HauteSeine";

  public function __construct()
  {

  	$this->_connexionMongo = new MongoDB\Driver\Manager("mongodb://$this->_host:$this->_port");

  }

  public function getAllDocuments()
  {

  	$query = new \MongoDB\Driver\Query([]);
  	$result = $this->_connexionMongo->executeQuery('CartePostale.HauteSeine', $query);
  	return $result;

  }

  public function getDocumentById($idDocument)
  {

    $id = new \MongoDB\BSON\ObjectId($idDocument);
    $filter = ['_id' => $id];
    $options = [];

    $query = new \MongoDB\Driver\Query($filter,$options);
    $result = $this->_connexionMongo->executeQuery('CartePostale.HauteSeine', $query);
    return $result;

  }

  public function gagnerExperience()
  {

  }
}

?>
