<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="../css/style.css">
     <link href="../lightbox2-2.11.1/dist/css/lightbox.css" rel="stylesheet" 
	/>
	<link href="../lightbox2-2.11.1/dist/css/lightbox.min.css" rel="stylesheet" />

	<script src="../lightbox2-2.11.1/dist/js/lightbox.js"></script>
    <script src="../lightbox2-2.11.1/dist/js/lightbox-plus-jquery.min.js"></script>


    <title>Accueil</title>
  </head>

<?php

include 'activite.php';
include 'menu.php';


$activite = new Activite();
$resultatQuery = $activite->getAllDocuments();


echo('<body> <div class="container">');
$i = 0;
foreach($resultatQuery as $document){
    
	echo('

		
		<div class="card" style="width: 18rem; ">

			
				<a id="linkPicture" class="example-image-link" href="'.$document->images.'" data-lightbox="example-1">
				<img src="'.$document->images.'" class="img-document" alt="...">
			</a>
				<div class="card-text">
					<p class="card-text-titre">'.$document->Titre.'</p>
					<div class="commune-date">
						<p class="card-text-commune">'.$document->Commune.'</p>
						<p class="card-text-date">'.$document->Date.'</p>
					</div>	
				</div>
			<form action="documentLook.php" method="Post">
					<input type="hidden" id="idDocument" name="idDocument" value="'.$document->_id.'">
				      <button type="submit" class="button-more">More</button>
				</form>
		</div>


		');

    if(++$i > 20) break;

}

echo('</div></body>');

include 'footer.php';

?>